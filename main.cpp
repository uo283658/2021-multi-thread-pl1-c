/*
 * Main.cpp
 *
 *  Parte 2.2 del trabajo grupal: Saúl Jou González, Guillermo García Mesa y Pelayo García Suárez.
 */

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>

using namespace cimg_library;

class Pixel // Clase donde se guarda los atributos de cada pixel para pasárselos en un solo argumento a la función en cada llamada
{
	private: data_t *pixelFR1, *pixelFR2, *pixelDR, *pixelFG1, *pixelFG2, *pixelDG, *pixelFB1, *pixelFB2, *pixelDB;
	private: int pixelSuperior, pixelInferior;

	public:
		Pixel()
		{
			setPixelFR1(0);
			setPixelFG1(0);
			setPixelFB1(0);
			setPixelFR2(0);
			setPixelFG2(0);
			setPixelFB2(0);
			setPixelDR(0);
			setPixelDG(0);
			setPixelDB(0);
			setPixelInferior(0);
			setPixelSuperior(0);
		}
	public:
		data_t *getPixelFR1(){
			return pixelFR1;
		}
	public:
		data_t *getPixelFR2(){
			return pixelFR2;
		}
	public:
		data_t *getPixelDR(){
			return pixelDR;
		}
	public:
		data_t *getPixelFG1(){
			return pixelFG1;
		}
	public:
		data_t *getPixelFG2(){
			return pixelFG2;
		}
	public:
		data_t *getPixelDG(){
			return pixelDG;
		}
	public:
		data_t *getPixelFB1(){
			return pixelFB1;
		}
	public:
		data_t *getPixelFB2(){
			return pixelFB2;
		}
	public:
		data_t *getPixelDB(){
			return pixelDB;
		}
	public:
		int getPixelSuperior(){
			return pixelSuperior;
		}
	public:
		int getPixelInferior(){
			return pixelInferior;
		}

	public:
		void setPixelFR1(double *pfr1){
			pixelFR1 = pfr1;
		}
	public:
		void setPixelFR2(double *pfr2){
			pixelFR2 = pfr2;
		}
	public:
		void setPixelDR(double *pdr){
			pixelDR = pdr;
		}
	public:
		void setPixelFG1(double *pfg1){
			pixelFG1 = pfg1;
		}
	public:
		void setPixelFG2(double *pfg2){
			pixelFG2 = pfg2;
		}
	public:
		void setPixelDG(double *pdg){
			pixelDG = pdg;
		}
	public:
		void setPixelFB1(double *pfb1){
			pixelFB1 = pfb1;
		}
	public:
		void setPixelFB2(double *pfb2){
			pixelFB2 = pfb2;
		}
	public:
		void setPixelDB(double *pdb){
			pixelDB = pdb;
		}
	public:
		void setPixelSuperior(int ps){
			pixelSuperior = ps;
		}
	public:
		void setPixelInferior(int pi){
			pixelInferior = pi;
		}
};

// Tipos de datos para las componentes de la imagen
typedef double data_t;

const char* SOURCE_IMG1      = "bailarina.bmp";
const char* SOURCE_IMG2      = "background_V.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

void* algoritmo(void* arg) // Función: cálculo de píxeles de destino
{
	Pixel p = *((Pixel*)arg);
	for (int i = p.getPixelInferior(); i < p.getPixelSuperior(); i++)
	{
		// R
		if ((255 - ((256 * (255 - *(p.getPixelFR2() + i))) / (*(p.getPixelFR1() + i) + 1))) > 255) *(p.getPixelDR() + i) = 255; // Si la componente se satura por encima de 255, asignar 255
		else if ((255 - ((256 * (255 - *(p.getPixelFR2() + i))) / (*(p.getPixelFR1() + i) + 1))) < 0) *(p.getPixelDR() + i) = 0; // Si la componente se satura por encima de 0, asignar 0
		else *(p.getPixelDR() + i) = (255 - ((256 * (255 - *(p.getPixelFR2() + i))) / (*(p.getPixelFR1() + i) + 1)));
		// G
		if ((255 - ((256 * (255 - *(p.getPixelFG2() + i))) / (*(p.getPixelFG1() + i) + 1))) > 255) *(p.getPixelDG() + i) = 255; // Si la componente se satura por encima de 255, asignar 255
		else if ((255 - ((256 * (255 - *(p.getPixelFG2() + i))) / (*(p.getPixelFG1() + i) + 1))) < 0) *(p.getPixelDG() + i) = 0; // Si la componente se satura por encima de 0, asignar 0
		else *(p.getPixelDG() + i) = (255 - ((256 * (255 - *(p.getPixelFG2() + i))) / (*(p.getPixelFG1() + i) + 1)));
		// B
		if ((255 - ((256 * (255 - *(p.getPixelFB2() + i))) / (*(p.getPixelFB1() + i) + 1))) > 255) *(p.getPixelDB() + i) = 255; // Si la componente se satura por encima de 255, asignar 255
		else if ((255 - ((256 * (255 - *(p.getPixelFB2() + i))) / (*(p.getPixelFB1() + i) + 1))) < 0) *(p.getPixelDB() + i) = 0; // Si la componente se satura por encima de 0, asignar 0
		else *(p.getPixelDB() + i) = (255 - ((256 * (255 - *(p.getPixelFB2() + i))) / (*(p.getPixelFB1() + i) + 1)));
	}
}

int main() {

	const int REP = 18; // Número de repeticiones de bucle
	const int HILOS = 12;
	pthread_t thread[HILOS]; // 12 hilos en una CPU de 6 núcleos

	// Comprobación de existencia de imágenes
	try
	{
		CImg<data_t> srcImage1(SOURCE_IMG1);
		CImg<data_t> srcImage2(SOURCE_IMG2);
	}
	catch(const std::exception& e)
	{
		printf("ERROR: No existen las imágenes solicitadas. Saliendo del programa...");
		return -1;
	}
	// Abrir archivo e inicialización de objetos
	CImg<data_t> srcImage1(SOURCE_IMG1);
	CImg<data_t> srcImage2(SOURCE_IMG2);

	data_t *pRsrc1, *pGsrc1, *pBsrc1; // Punteros a las componentes R, G y B de src1
	data_t *pRsrc2, *pGsrc2, *pBsrc2; // Punteros a las componentes R, G y B de src2
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Puntero a los nuevos píxeles de la imagen
	uint width, height; // Anchura y altura de la imagen
	uint nComp; // Número de componentes de la imagen

	// Preparación de variables para el algoritmo
	struct timespec tStart, tEnd; // Tiempo inicial y final
	double dElapsedTimeS; // Tiempo transcurrido

	// Comprobación de misma dimensión en las dos imágenes
	if (srcImage1.width() != srcImage2.width() || srcImage1.height() != srcImage2.height())
	{
		printf("ERROR: Las dimensiones de las dos imágenes no coinciden. Cerrando programa...");
		return -1;
	}

	srcImage1.display(); // Muestra la imagen fuente
	width  = srcImage1.width(); // Consiguiendo información desde la imagen fuente
	height = srcImage1.height();
	nComp  = srcImage1.spectrum(); // Número de componentes de la imagen fuente
				// Valores comunes para el espectro (número de componentes de imagen):
				//  Imágenes en blanco y negro = 1
				//	Imágenes de color normal = 3 (RGB)
				//  Imágenes de color especial = 4 (RGB y canal alpha/transparencia)
	srcImage2.display();

	// Asignar espacio de memoria para el destino de los componentes de la imagen
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Asignar imagen de destino");
		exit(-2);
	}

	// Punteros a las matrices de componentes de la imagen fuente 
	pRsrc1 = srcImage1.data(); // pRcomp apunta a la matriz de componente R de src1
	pGsrc1 = pRsrc1 + height * width; // pGcomp apunta a la matriz de componente G de src1
	pBsrc1 = pGsrc1 + height * width; // pBcomp apunta a la matriz de componente B de src1

	pRsrc2 = srcImage2.data(); // pRcomp apunta a la matriz de componente R de src2
	pGsrc2 = pRsrc2 + height * width; // pGcomp apunta a la matriz de componente G de src2
	pBsrc2 = pGsrc2 + height * width; // pBcomp apunta a la matriz de componente B de src2

	// Punteros a las matrices RGB de la imagen de destino
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	// Medición de tiempo inicial (se incluye también el tiempo de creación y destrucción de los hilos)
	printf("Ejecutando tarea    : ");
	fflush(stdout);

	if (clock_gettime(CLOCK_REALTIME, &tStart) != 0)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	// Algoritmo
	// Blend: Blacken Mode (Algoritmo Nº 12)
	for (uint j = 0; j < REP; j++) // 18 repeticiones con tal de aumentar el tiempo de ejecución del programa
	{	
		Pixel *infoHilos = (Pixel *) malloc (HILOS*sizeof(Pixel)); // Reserva de memoria para el vector infoHilos, contendrá información individual para cada hilo
		for (uint i = 0; i < HILOS; i++) // Bucle para iniciar cada hilo
		{
			if (i == HILOS-1 && (width*height) % HILOS != 0) // Si es el último hilo y el número de píxeles asignados a cada hilo no es igual para todos...
			{
				infoHilos[i].setPixelSuperior(width*height); // Último hilo va hasta el último pixel
				infoHilos[i].setPixelInferior(infoHilos[i-1].getPixelSuperior()); // Último hilo va desde el último pixel del penúltimo hilo
			}
			else // Si no es el último hilo...
			{
				infoHilos[i].setPixelSuperior(((width*height)/HILOS) * (i+1));
				infoHilos[i].setPixelInferior(infoHilos[i].getPixelSuperior() - ((width*height)/HILOS));
			}
			// Asignaciones de atributos comunes a todos los hilos
			infoHilos[i].setPixelFR1(pRsrc1);
			infoHilos[i].setPixelFG1(pGsrc1);
			infoHilos[i].setPixelFB1(pBsrc1);

			infoHilos[i].setPixelFR2(pRsrc2);
			infoHilos[i].setPixelFG2(pGsrc2);
			infoHilos[i].setPixelFB2(pBsrc2);

			infoHilos[i].setPixelDR(pRdest);
			infoHilos[i].setPixelDG(pGdest);
			infoHilos[i].setPixelDB(pBdest);

			if (pthread_create(&thread[i], NULL, algoritmo, &infoHilos[i]) != 0) // Creación e inicialización del hilo i
			{
				fprintf(stderr, "ERROR al crear el hilo Nº %d\n", i);
				return EXIT_FAILURE;
			}
		}

		printf("Hilo principal en modo espera...\n");
		for (uint i = 0; i < HILOS; i++)
		{
			pthread_join(thread[i], NULL);	// Esperar a terminar la ejecución de los 12 hilos
		}
		printf("Todos los hilos han terminado su ejecución.\n");

		free(infoHilos); // Liberación de memoria del vector de tipo Pixel
	}

	// Medición de tiempo final
	if (clock_gettime(CLOCK_REALTIME, &tEnd) != 0)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	printf("Terminado\n");

	// Medimos el tiempo transcurrido
	dElapsedTimeS = tEnd.tv_sec - tStart.tv_sec;
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Tiempo transcurrido    : %f s.\n", dElapsedTimeS);
		
	// Creamos un nuevo objeto imagen con los píxeles calculados
	// En caso de imágenes de color normal usar nComp=3,
	// En caso de usar imágenes en blanco y negro usar nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);

	return 0;
}
